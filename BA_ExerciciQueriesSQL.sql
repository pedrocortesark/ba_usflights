#1_ Quantitat de registres de la taula de vols
SELECT COUNT(flightID) 
AS total 
FROM flights;

#2_ Retard promig de sortida i arribada segons l’aeroport origen
SELECT Origin, AVG(ArrDelay) AS prom_arrivades, AVG(DepDelay) AS prom_sortides
FROM flights
GROUP BY Origin;

#3_ Retard promig d’arribada dels vols, per mesos, anys i segons l’aeroport origen
SELECT Origin, colYear, colMonth, AVG(ArrDelay) AS prom_arrivades
FROM flights
GROUP BY Origin, colYear, colMonth
ORDER BY Origin, colYear ASC, colMonth ASC;

#4_ Retard promig d’arribada dels vols, per mesos, anys i segons l’aeroport ori
SELECT usairports.City AS City, colYear, colMonth, AVG(ArrDelay) AS prom_arrivades
FROM flights
LEFT JOIN usairports ON (flights.Origin = usairports.IATA)
GROUP BY City, colYear, colMonth
ORDER BY City, colYear ASC, colMonth ASC;
 
#5_ Les companyies amb més vols cancelats, per mesos i any. A més, han d’estar ordenades de forma que les companyies amb més cancel·lacions apareguin les primeres
SELECT UniqueCarrier, colYear, colMonth, AVG(ArrDelay) AS avg_delay, SUM(Cancelled) AS total
FROM flights
GROUP BY UniqueCarrier, colYear, colMonth
ORDER BY total DESC;
 
#6_ L’identificador dels 10 avions que més distància han recorregut fent vols.
SELECT TailNum, SUM(Distance) AS totaldistance
FROM flights
WHERE TailNum != ""
GROUP BY TailNum
ORDER BY totaldistance DESC;

#7_ Companyies amb el seu retard promig només d’aquelles les quals els seus vols arriben al seu destí amb un retràs promig major de 10 minuts.
SELECT UniqueCarrier, AVG(ArrDelay) AS avgDelay
FROM flights
GROUP BY UniqueCarrier
HAVING avgDelay > 10
ORDER BY avgDelay desc; 